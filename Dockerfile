FROM mautic/mautic:v4-apache

ENV MAUTIC_PERSISTENT_VOLUME_PATH /data

ENV MAUTIC_RUN_INITIAL_INSTALL false

#  Disabling for now, we don't need it
#RUN apt-get update && apt-get install -y supervisor
# COPY common/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

RUN  tar cf - --one-file-system -C /usr/src/mautic . | tar xf -

RUN rm /makeconfig.php

# Overriding the makeconfig.php file to use ours
# which makes use of the correct the persistent volume path
COPY common/makeconfig.php /makeconfig.php

# Pushing paths_local.php to point to our persistent volume path
COPY common/paths_local.php /var/www/html/app/config/paths_local.php

RUN chown -R www-data:www-data /var/www/html/app/config

# Overriding the crontab file to use ours because we commented out bugged command (@reboot...)
COPY common/mautic.crontab /etc/cron.d/mautic
RUN chmod 644 /etc/cron.d/mautic


# Overriding the entrypoint file to use ours
# Ours will make sure the persistent volume path is used whenevever
# necessary, it also handles the copying of the themes folder do the persistent volumes
RUN rm /entrypoint.sh
COPY common/docker-entrypoint.sh /entrypoint.sh

RUN ["chmod", "+x", "/entrypoint.sh"]
